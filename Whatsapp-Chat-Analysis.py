from sys import argv
from os import path

message_count_dict = {}
message_word_count_dict = {}
words_used_dict = {}
name = u""

def count_messages(name, message):
    # If key exists increment, else initialize with 0+1
    message_count_dict[name] = message_count_dict.get(name, 0) + 1 


def count_words(name, message):
    message_len = len(message.split(' '))
    message_word_count_dict[name] = message_word_count_dict.get(name, 0) + message_len


def collect_words_used(name, message):
    if name not in words_used_dict:
        words_used_dict[name] = {}

    for word in message.split(' '):
        words_used_dict[name][word] = words_used_dict[name].get(word, 0) + 1


# Example: [17.10.19, 22:21:37] Manu: The cake is a lie!
def parse_line(line):
    if line[0] == '[':  # Very simple skip of lines of multi line messages (that don't begin with a timestamp)
        global name
        name, message = line.split(']', 1)[1].split(':', 1)
        name = name.strip()
        message = message.strip()

        count_messages(name, message)   # Only count the message, if it beginns with a timestamp.
    else:
        message = line # [TO MAX]: What do you think about that? +Readability or +Redundancy? or both? worth it?

    count_words(name, message)
    collect_words_used(name, message)
    
        
if __name__ == "__main__":
    # === Argument Handling ===
    if len(argv) == 1:
        if not path.exists("_chat.txt"):
            print(
            "[Warning] Couldn't find the default _chat.txt WhatsApp chat log file in the same directory as this program.\n"
            "[1] Export the chat log from the Whatsapp mobile app.\n"
            "    It will most likely be called '_chat.txt'\n"
            "[2] Give that file as a an argument to the program, like this (without the '>'):\n"
            "    > python _my_chat_log_file.txt"
            )
            exit()
        filename = "_chat.txt"
    elif len(argv) == 2:
        filename = argv[1]
    else:
        print("[Error]: Too many arguments") # TODO: Use getopt for -h and better argument handling and more precise instructions on how to use the program.

    # === Start of Main Code ===
    with open(filename, 'r') as chat_log_fd:
        line = chat_log_fd.readline()
        while line: # Read lines till EOF
            parse_line(line) # Extract information out of the line read
            line = chat_log_fd.readline()

    # === Output ===
    print("Number of messages sent by")
    for user in message_count_dict:
        print("\t{}: {}".format(user, message_count_dict[user]))

    print("Number of words sent by")
    for user in message_word_count_dict:
        print("\t{}: {}".format(user, message_word_count_dict[user]))

    for user in words_used_dict:
        pass # TODO: To be implemented
        # print("User: {} - Words used".format(user))
        # for word, count in words_used_dict[user]:
            # print("\t{}: {}".format(word, count))
